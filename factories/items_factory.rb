FactoryBot.define do
  factory :item do
    name { Faker::Food.unique.ingredient + Time.now.to_i.to_s }
    comment { Faker::Food.metric_measurement }
    count { 1 + rand(60) }
    bought { false }
  end
end
