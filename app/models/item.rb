class Item < ApplicationRecord
  validates :name, presence: true, length: {
    in: 2..200
  }

  validates :comment, presence: false, length: {
    in: 0..200
  }, if: proc { |i| !i.comment.nil? }

  validates :count, presence: true, numericality: { greater_than: 0 }

  before_save :default_values

  def default_values
    self.bought ||= false
  end
end
