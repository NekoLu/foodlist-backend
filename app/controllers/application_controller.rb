class ApplicationController < ActionController::API
  rescue_from StandardError,
              with: :render_standard_error

  def render_standard_error error
    puts error
    api_render errors: { server: [error] }, code: 500, status: false
  end

  def api_render(args = {})
    defaults = {
      status: true,
      code: 200,
      errors: {},
      result: {}
    }
    args = defaults.merge(args)
    render json: {
      status: args[:status] ? 'ok' : 'fail',
      code: args[:code],
      errors: args[:errors],
      result: args[:result]
    }, status: args[:code]
  end
end
