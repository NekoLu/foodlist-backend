class ItemsController < ApplicationController
  def index
    @items = if params[:all]
               Item.all
             elsif params[:bought]
               Item.where(bought: true)
             else
               Item.where(bought: false)
             end
    api_render result: { items: @items.reverse }
  end

  def create
    @item = Item.new(item_params)
    unless @item.valid?
      return api_render errors: @item.errors, code: 403, status: false
    end

    @item.save
    api_render code: 201
  end

  def update
    @item = Item.find_by_id(params[:id])
    unless @item.update(item_params)
      return api_render errors: @item.errors, code: 403, status: false
    end

    api_render result: { item: @item }
  end

  private

  def item_params
    params[:item] ||= { name: nil, comment: nil, count: nil, bought: nil }

    params.require(:item).permit(:name, :comment, :count, :bought)
  end
end
