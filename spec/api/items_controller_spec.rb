require 'rails_helper'

describe ItemsController, '#index', type: :request do
  describe 'without params' do
    let!(:items) { create_list(:item, 10) }
    let!(:item) { create(:item, bought: true) }
    before { get '/items' }

    include_examples 'status and code', 'ok', 200

    it 'should: have all unbought items inside' do
      expect(json_response[:result][:items].size).to eq 10
    end
  end
  describe 'with bought=true' do
    let!(:items) { create_list(:item, 10) }
    let!(:item) { create(:item, bought: true) }
    before do
      get '/items', params: {
        bought: true
      }
    end

    include_examples 'status and code', 'ok', 200

    it 'should: have all bought items inside' do
      expect(json_response[:result][:items].size).to eq 1
    end
  end

  describe 'with all=true' do
    let!(:items) { create_list(:item, 10) }
    let!(:item) { create(:item, bought: true) }
    before do
      get '/items', params: {
        all: true
      }
    end

    include_examples 'status and code', 'ok', 200

    it 'should: have all bought items inside' do
      expect(json_response[:result][:items].size).to eq 11
    end
  end
end

describe ItemsController, '#create', type: :request do
  describe 'with correct data' do
    let!(:name) { Faker::Food.unique.ingredient + Time.now.to_i.to_s }
    let!(:comment) { Faker::Food.metric_measurement + Time.now.to_i.to_s }
    before do
      post '/items', params: {
        item: {
          name: name,
          comment: comment,
          count: 1
        }
      }
    end

    include_examples 'status and code', 'ok', 201

    it 'should: create item with right name' do
      expect(Item.last.name).to eq name
    end

    it 'should: create item with right comment' do
      expect(Item.last.comment).to eq comment
    end

    it 'should: create item with right count' do
      expect(Item.last.count).to eq 1
    end

    it 'should: create item with bought=false' do
      expect(Item.last.bought).to be_falsey
    end
  end

  describe 'with incorrect data' do
    let!(:name) { Faker::Food.unique.ingredient + Time.now.to_i.to_s }
    let!(:comment) { Faker::Food.metric_measurement + Time.now.to_i.to_s }
    before do
      post '/items', params: {
        item: {
          name: 'a' * 201,
          comment: 'a' * 201,
          count: -1
        }
      }
    end

    include_examples 'status and code', 'fail', 403
    include_examples 'have errors', { name: 1, comment: 1, count: 1 }
  end
end

describe ItemsController, '#update', type: :request do
  describe 'with correct data' do
    let!(:item) { create(:item)}
    let!(:name) { Faker::Food.unique.ingredient + Time.now.to_i.to_s }
    let!(:comment) { Faker::Food.metric_measurement + Time.now.to_i.to_s }
    let!(:count) {Time.now.to_i}
    before do
      patch "/items/#{item.id}", params: {
        item: {
          name: name,
          comment: comment,
          count: count,
          bought: true
        }
      }
    end

    include_examples 'status and code', 'ok', 200

    it 'should: update item name' do
      expect(Item.find_by_id(item.id).name).to eq name
    end

    it 'should: update item comment' do
      expect(Item.find_by_id(item.id).comment).to eq comment
    end

    it 'should: update item count' do
      expect(Item.find_by_id(item.id).count).to eq count
    end

    it 'should: update item bought' do
      expect(Item.find_by_id(item.id).bought).to be_truthy
    end
  end
end