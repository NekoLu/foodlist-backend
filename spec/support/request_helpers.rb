module Request
  module JsonHelpers
    def json_response
      begin
        @json_response ||= JSON.parse(response.body, symbolize_names: true)
      rescue JSON::ParserError
        raise KeyError
      end
    end
  end
end