RSpec.shared_examples 'status and code' do |status, code|
  it "should: return HTTP status #{code}" do
    expect(response).to have_http_status(code), "expected #{code}, got #{response.status}"
  end

  it "should: have #{code} code" do
    expect(json_response[:code]).to eq(code), "expected #{code}, got #{response.status}"
  end

  it "should: have #{status} status" do
    expect(json_response[:status]).to eq(status), "expected #{status}, got #{json_response[:status]}"
  end

  if status == 'ok'
    it 'should: have no errors' do
      expect(json_response[:errors].empty?).to be(true), "expected no errors, got #{json_response[:errors]}"
    end
  end
end
