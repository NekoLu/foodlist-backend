RSpec.shared_examples 'have errors' do |errors|
  errors.keys.each do |field|
    count = errors[field]
    it "should: have #{count} #{field} error#{count > 1 ? 's' : ''}" do
      expect(json_response[:errors][field].size).to eq count
    end
  end
end
