require 'rails_helper'

RSpec.describe Item, type: :model do
  it 'should not: be valid without name' do
    item = build(:item, name: nil)
    expect(item.valid?).to be_falsey
  end

  it 'should not: be valid with too short name' do
    item = build(:item, name: 'a')
    expect(item.valid?).to be_falsey
  end

  it 'should not: be valid with too long name' do
    item = build(:item, name: 'a' * 201)
    expect(item.valid?).to be_falsey
  end

  it 'should: be valid without comment' do
    item = build(:item, comment: nil)
    expect(item.valid?).to be_truthy
  end

  it 'should not: be valid with too long comment' do
    item = build(:item, name: 'a' * 201)
    expect(item.valid?).to be_falsey
  end

  it 'should not: be valid with count less then zero' do
    item = build(:item, count: -1)
    expect(item.valid?).to be_falsey
  end

  it 'should not: be valid without count' do
    item = build(:item, count: nil)
    expect(item.valid?).to be_falsey
  end
end
