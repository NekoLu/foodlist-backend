class CreateItems < ActiveRecord::Migration[5.1]
  def change
    create_table :items do |t|
      t.string :name
      t.integer :count
      t.text :comment
      t.boolean :bought

      t.timestamps
    end
  end
end
