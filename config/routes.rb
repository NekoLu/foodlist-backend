Rails.application.routes.draw do
  # get 'items/index'
  #
  # get 'items/create'
  #
  # get 'items/update'
  #
  resources :items, only: %i[index create update]

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
